APP_NAME = react-demo
DEPLOY_PACKAGE_DIR = ${APP_NAME}.codedeploy

stop:
	-docker rm -f ${APP_NAME}

run: stop
	docker build -t ${APP_NAME} .
	docker run -d -p 3000:3000 --name ${APP_NAME} ${APP_NAME}

build:
	sleep 30
	npm ci --prod
	npm run build

clean:
	-rm -rf ${DEPLOY_PACKAGE_DIR}
	-rm -rf build
	-rm -f ${APP_NAME}.docker.zip
	-rm -f ${APP_NAME}.docker
	-rm -f ${APP_NAME}.codedeploy.zip
	-docker rm -f ${APP_NAME}

package:
	rm -f ${DEPLOY_PACKAGE_DIR}.zip; rm -rf ${DEPLOY_PACKAGE_DIR}; mkdir ${DEPLOY_PACKAGE_DIR}
	docker build -t ${APP_NAME} .
	docker save --output ${DEPLOY_PACKAGE_DIR}/${APP_NAME}.docker ${APP_NAME}
	cp appspec.yml ${DEPLOY_PACKAGE_DIR}
	cp -r scripts ${DEPLOY_PACKAGE_DIR}
	cd ${DEPLOY_PACKAGE_DIR}; zip -r -9 ../${APP_NAME}.codedeploy.zip .
	rm -f ${APP_NAME}.docker

.PHONY: clean build package publish ci