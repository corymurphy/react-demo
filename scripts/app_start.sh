
cd /app/react-demo;

echo "$(date)" > ~/app_start_date;
echo "$(pwd)" > ~/codedeploy_pwd;

make run

# docker rm -f react-demo > /dev/null
# docker build -t react-demo .
# docker run -d -p 3000:3000 --name react-demo react-demo
